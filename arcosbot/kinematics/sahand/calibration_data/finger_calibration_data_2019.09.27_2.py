from numpy import array,pi
angle_calibration_data_right =     [array( [0.0, 0.,0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array([-0.02104493, -0.25924269, -0.15307158]),
                                    array( [0.0, 0.,0.])]

torque_calibration_factors_right = [array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([-0.70427423, -0.48013397, -6.49669078]),
                                    array([1.0,1.,1.])]

angle_calibration_data_left =      [array( [0.0, 0.,0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.])]

torque_calibration_factors_left =  [array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.])]

