from numpy import array,pi
angle_calibration_data_right =     [array( [0.0, 0.,0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array([ 0.03983217, -0.26650326, -0.15055835]),
                                    array( [0.0, 0.,0.])]

torque_calibration_factors_right = [array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([ 1.53985951, 0.57153895, 0.56941139]),
                                    array([1.0,1.,1.])]

angle_calibration_data_left =      [array( [0.0, 0.,0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.]),
                                    array( [0.0, 0.,0.])]

torque_calibration_factors_left =  [array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.]),
                                    array([1.0,1.,1.])]


