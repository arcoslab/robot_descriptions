from PyKDL import *
from math import pi
from lin_inter import a5a6_limit
from lin_inter import limit_kuka_to_negative
from lin_inter import fix_limit
from numpy import array

# main configuration
arm_type = 'lwr'
nJoints = 7
torso_joints= []
torso_instance = "left"

arm_instance = 'right'

# when set to False, the wlds solvers joint weights get modulated
# according to proximity to joint limits
disable_smart_joint_limit_avoidance = False

#yarp robot-arm port basename, must match the file name: config-arm-side
robotarm_portbasename= "/"+arm_type+"/"+arm_instance

# bridge communication
qin_portname = "/qin"
qcmded_portname = "/qcmded"
qcmd_portname = "/qcmd"

#inverse kinematics parameters

# low lambda (0.01): high tracking accuracy
# high lambda (0.5): high singularity clearance
ik_lambda = 0.10 # how much should singularities be avoided?

#TODO: these weights are not working right
ik_weightTS = (1.0,)*6  # how much should the cartesian goal directions be pursued?
ik_weightJS = (1.0,)*nJoints # how much should each joint be used to accomplish the goal?

speedScale=1.0

#max joint speed
max_vel=41.0*pi/180  # 30.0
max_vel=21.0*pi/180  # 30.0
#**************************for inertia this may change
max_vel=441.0*pi/180
max_vel=15.0*pi/180  # 30.0
max_vel_joint_sim=180.0*pi/180.0


#initial joint position
initial_joint_pos=[0.0,-1.2,0.7,1.4,0.35,-1.4,0.0]
initial_joint_pos=[0.0,0.0,0.0,0.0,0.0,pi/3,pi/2]
initial_joint_pos=[0.6, 0.91, 0.4, 1.6, 1.0, -0.95, 0.3] # for robot arm in wood mounting 
initial_joint_pos=[0.0, -0.57, 0.0, 1.4, -0.7, 0.95, 1.0] # With Andres Alvarado torso

# joint_p_controller
jpctrl_kp=1.5
jpctrl_kp=4.5
jpctrl_kp=6.5
jpctrl_kp=0.8 #original slow sim
jpctrl_kvp=0.8 #original slow sim
#jpctrl_kp=1.8
#jpctrl_kvp=1.8
jpctrl_inertia=0.01 #original slow sim
#jpctrl_inertia=0.005

# arm configuration
arm_segments = [
    Segment(Joint(Joint.None),
#            Frame(Rotation.RPY(0.71178, 0.85723, -0.71169),Vector(0.0, 0, 1.156)  )),
            Frame(Rotation.RotX(45.0*pi/180.0)*Rotation.RotZ(30.0*pi/180.0),Vector(0.0, -0.1327, 1.02)  )),
    Segment(Joint(Joint.None),
            Frame(Rotation.Identity(), Vector(0.0, 0.0, 0.11))),
    Segment(Joint(Joint.RotZ),
            Frame(Rotation.RotX(-pi/2), Vector(0.0, 0.0, 0.20))),
    Segment(Joint(Joint.RotZ, -1),
            Frame(Rotation.RotX(pi/2), Vector(0.0, -0.20, 0.0))),
    Segment(Joint(Joint.RotZ),
            Frame(Rotation.RotX(pi/2), Vector(0, 0, .20))),
    Segment(Joint(Joint.RotZ, -1),
            Frame(Rotation.RotX(-pi/2), Vector(0, 0.2, 0))),
    Segment(Joint(Joint.RotZ),
            Frame(Rotation.RotX(-pi/2), Vector(0, 0, 0.19))),
    Segment(Joint(Joint.RotZ, -1),
            Frame(Rotation.RotX(pi/2), Vector(0, -0.078, 0.0))),
    Segment(Joint(Joint.RotZ),
        Frame(Rotation.RotZ((180.0+45)*pi/180.0)*Rotation.RotX(pi/2)*Rotation.RotY(pi), Vector(0.075, -0.075, -0.094))),
]


arm_limits= [[-169.5*pi/180, 169.5*pi/180],
             [-119.5*pi/180, 119.5*pi/180],
             [-169.5*pi/180, 169.5*pi/180],
             [-119.5*pi/180, 119.5*pi/180],
             [-169.5*pi/180, 169.5*pi/180],
             [-119.5*pi/180, 119.5*pi/180],
             [-169.5*pi/180, 169.5*pi/180]]


#Extra margins in the limits for limit avoidance. This is necessary for all robots to define
arm_extralimit_margin=1.0*pi/180
arm_extralimits=[[x[0]+arm_extralimit_margin,x[1]-arm_extralimit_margin] for x in arm_limits]

#control loop speed in seconds
rate=0.01
fast_mode=False
virtual_rate=0.01
fast_rate=0.005

#special A5 and A6 joint limits
limitsA6=[  #table for limits on A6 when changing A5. Data: A5_angle, A6minlim, A6maxlim
    [-120, -170, -5],
    [-100, -170, -10],
    [-80, -170, -5],
    [-60, -170, 0],
    [-40, -170, 15],
    [-20, -170, 70],
    [-17, -170, 70],
    [-15, -170, 170],
    [-10, -170, 170],
    [0, -170, 170],
    [20, -170, 170],
    [30, -140, 130],
    [40, -105, 130],
    [60, -95, 130],
    [80, -90, 130],
    [100, -90, 130],
    [120, -90, 110]
]

limitsA6=[map(float,i) for i in limitsA6]
limitsA5= [ #table for limits on A5 when changing A6. Data: A6_angle, A5minlim, A5maxlim
    [-170, -120, 10],
    [-150, -120, 25],
    [-130, -120, 30],
    [-110, -120, 35],
    [-100, -120, 45],
    [-90, -120, 120],
    [-70, -120, 120],
    [-50, -120, 120],
    [-30, -120, 120],
    [-10, -120, 120],
    [-5, -85, 120],
    [0, -55, 120],
    [10, -40, 120],
    [30, -30, 120],
    [50, -25, 120],
    [70, -20, 120],
    [90, -5, 120],
    [110, -5, 120],
    [130, -90, 95],
    [140, -95, 95],
    [150, -110, 10],
    [170, -120, 5]
]

limitsA5=[map(float,i) for i in limitsA5]
#limitsA5=[[i[0],limit_kuka_to_negative(i[1]),limit_kuka_to_negative(i[2])] for i in limitsA5]
limitsA5=[fix_limit(i) for i in limitsA5]

#Some safety distance to the hand limits
limitsA5=[[i[0], i[1]+5, i[2]-5] for i in limitsA5]
limitsA6=[[i[0], i[1]+5, i[2]-5] for i in limitsA6]

def updateJntLimits(jnt_pos):
    a5angle=jnt_pos[5]
    a6angle=jnt_pos[6]
    a5limits,a6limits=a5a6_limit(a5angle*360/(2*pi),a6angle*360/(2*pi),limitsA5,limitsA6)
    joint_limits=arm_extralimits
    joint_limits[5]=map(lambda x: x*2*pi/360, a5limits)
    joint_limits[6]=map(lambda x: x*2*pi/360, a6limits)
    return joint_limits

def update_joint_weights(q,qdot):
    return array([1.0]*nJoints)

#for roboview
segments=arm_segments
limits_min=[i[0] for i in arm_limits]
limits_max=[i[1] for i in arm_limits]
